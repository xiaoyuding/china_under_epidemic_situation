# 疫情下的中国

#### 参与方式
上传你所拍摄的你周边的环境或人抗击疫情的照片（防护措施） 

#### 项目介绍 
2020 的开头很难，本该欢庆的日子，却被肆虐的病毒打破。随处可见的测温点，空荡荡的街道，为买口罩而排出的长长队伍，各式各样的封路手法，不断开往武汉的物资运输车辆，响应着“宅在家也是为社会做贡献”的号召用宅在家里人们...在这段时光里，很多暖心、辛酸、有趣瞬间被我们记录了下来，希望能够将这些照片都收集起来，让更多人感受到“众志成城”的力量，愿阴霾过后，春暖花开～

本仓库并不要求你贡献代码，没有编程技能都可以参加。你可以通过尝试上传照片学习从克隆项目，创建分支，提交和同步修改，到合并分支请求的整套流程。

#### 参与要求
1.  创建文件夹并上传图片的形式
2.  请上传自己拍摄的图片，转载他人图片请获得允许并备注来源，侵删。
3.  请给贡献的文件夹取一个有意义的文件名，文件名长度不得超过 20 个字符（可使用你的 ID 名命名文件夹），允许提交多个作品。具体形式可参考参赛作品：湖南的小镇
5.  如果需要在你的文件夹内创建描述文件,请尽量使用 Markdown 语法来编写
6.  图片请尽量压缩大小并本地上传, 但需要足够清晰

关于 Fork 和 Pull Requests 的用法请阅读 https://gitee.com/help/articles/4128

